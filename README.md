# Custom Package "Hello World" Laravel 5 #

ini adalah custom package untuk laravel versi 5.* Untuk Menampilkan Kata "Hello World".

## Struktur Folder ##

Buat foder baru pada projek utama laravel yang sudah dibuat, Tambahkan pada folder 


```
#!cmd

vendor/nama-author/src 
```


*nama-author akan digunakan sebagai nama package yang nantinya akan digunakan 

pada directori */src* tersebut jalankan perintah 
```
#!cmd

composer init
```
pada terminal, perintah tersebutaalah perintah untuk membuat file *composer.json*
tambahkan pada file *composer.json* 


```
#!cmd

"autoload": {
        "psr-4": {
            "HelloWorld\\": "vendor/nama-author/src/"
        }
    },
```
buka terminal dan arahkan ke direktori package


```
#!cmd

cd C:\xampp\htdocs\nama-projek\vendor\nama-author\
```


```
#!python


composer dumpautoload
```






## Membuat Provider ##

masuk ke *Root* folder project jalankan command 

```
#!cmd

php artisan make:provider nama-authorServiceProvider
```

buka direktori 
```
#!cmd

app/Provider
```
 pada direktori utama, cari file dengan nama 
```
#!command

nama-authorServiceProvider
```
 copy kan file tersebut ke direktori *vendor/nama-author/src*.

# Membuat Controller #

Fungsi dari controler yang akan dibuat disini digunakan untuk membuat fungsi "Hello World" yang akan dipakai di laravel nantinya

buat file baru di folder */src*


```
#!dir

helloWorldController
```

buka file tersebut isikan kode seperti berikut

![111.PNG](https://bitbucket.org/repo/bbLRMn/images/53575353-111.PNG)

# Cara penggunaan #

pada file 
```
#!dir

config/app.php
```

Tambahkan kode berikut


```
#!php

'providers' => [

......
        'Helloworld\HelloworldServiceProvider',
......

],
'aliases' => [
......
        'Sample' => 'Helloworld\HelloworldController'
......
],
```
pada controller, tambahkan kode berikut

![222.PNG](https://bitbucket.org/repo/bbLRMn/images/1363713700-222.PNG)

pada view, buat kode untuk menampilkan isi dari variabel text


```
#!php

{{ $text }}
```