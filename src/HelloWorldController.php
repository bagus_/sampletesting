<?php namespace HelloWorld;
use App\Http\Controllers\Controller;

class HelloWorldController extends Controller {

  public function __construct() {
    $this->middleware('auth');
  }

  /**
  * Display a listing of the resource.
  *
  * @return Response
  */
  public static function index()
  {
    return 'HelloWorld';
  }

  public static function myView(){
    return view('Hello::Hello');
  }
}