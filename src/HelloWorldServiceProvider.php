<?php namespace HelloWorld;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
class HelloWorldServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
		// require __DIR__ . '/../vendor/autoload.php';
		AliasLoader::getInstance()->alias('HelloWorld', 'HelloWorld\HelloWorldServiceProvider');

		$this->loadViewForm(__DIR__.'./../view', 'Hello');

		$this->publishes([
            __DIR__.'./../view' => base_path('resources/views/'),
        ]);
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
		// $this->app->make('Gocanto\SimpleAdmin\SimpleAdminController');
		// include __DIR__.'/routes.php';
		// $this->app->alias('Sample', 'Sample\SampleController');
	}

}
